

<?php
session_start();

include "database.php";

// Om något fält i login-formuläret inte fyllts sker en omdirigering till index.php .
// Om båda fälten i login-formuläret är ifyllt öppnas en anslutning till databasen.
if( isset($_POST["login"]) ) {
		if( empty($_POST["user"]) || empty($_POST["pass"]) ) {
			$_SESSION ["login"] = "notset";
			header ("Location: index.php?error");
		}elseif( !empty($_POST["user"]) && !empty($_POST["pass"]) ) {
		
		// Ansluter till databas
		$conn = new mysqli($server, $username, $password, $database);
		// Kollar att anslutningen fungerar
		if ($conn->connect_error) {
		    die("Anslutningen misslyckades: " . $conn->connect_error);
		    exit;
		}
		// Tar bort skadliga tecken
		$user = strip_tags($_POST["user"]); 
		$pass = strip_tags($_POST["pass"]);
		// Skapar ett prepared statement
		$stmt = $conn->stmt_init();
		// Om queryn är korrekt så körs den mot databasen, raden som har rätt username
		// sparas i variablarna
		if( $stmt->prepare( "SELECT * FROM members WHERE username = '{$user}'") ) {
			$stmt->execute();
			$stmt->bind_result($id, $un, $up, $upic);
			$stmt->fetch();
		}
		// Kollar om lösenordet från inloggnings-formuläret stämmer med det krypterade lösnordet i databasen
		// och isf skickar vidare till dashboard.php. Annars tillbaka till index.php
		if($id != 0 && ( password_verify($pass, $up) == TRUE) ) {
			setcookie( "username", $un, time() + 3600 );
			$_SESSION ["logged_in"] = TRUE;
			$_SESSION["userid"] = $id;
			$_SESSION["username"] =	$un;
			$_SESSION["userpic"] =	$upic;
			$_SESSION["newuser"] =	FALSE;
			header ("Location: dashboard.php");
		}else{
			$_SESSION ["no_match"] = TRUE;

			header ("Location: index.php");

		}
	$conn->close();		
	}		
} 





?>
