<?php

session_start();

// Tömmer session-arrayen på information
session_destroy();
// Ställer om cookien så att den inte gäller längre
setcookie("username", " ", time() - 3600 );
// Omdirigerar till index.php
header ("Location: index.php");
exit;
?>