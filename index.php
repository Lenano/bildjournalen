<?php

session_start();


$title = "Startsida";
include "top.php";

?>

		<div class="container">	

			<!-- #####  Formulär för inloggning ##### -->
			<div class="item1">	
				<div>
					<h1> Logga in: </h1>
				</div>

		    	<form method="post" action="logincheck.php">	
					<input type="text" name="user" placeholder="Användarnamn"><br>	
					<input type="password" name="pass" placeholder="Lösenord"><br> 	
					<input type="submit" name="login" placeholder="Logga in" class="submit"><br> 	
				</form>	
				
				<?php

				if( !empty($_SESSION["login"] ) || !empty( $_SESSION["pass"] ) ){

					// ##### Om båda fälten i inloggningsformuläret inte är ifyllda dyker en varningstext upp #####
					if ($_SESSION["login"] == "notset") {  
						echo "<p class='warning'>Inloggningen misslyckades.<br> Fyll i både användarnamn och lösenord!</p>";
						$_SESSION["login"]= NULL; // Tar bort varningstext om sidan laddas uppdateras
					}
				}

				// ##### Varningstext om lösenordet inte matchar #####
				if ( (isset($_SESSION["no_match"]) && $_SESSION["no_match"] == TRUE)  ){
					echo "<p class='warning'> Lösenordet stämmer inte </p>";
					$_SESSION["no_match"] = NULL;
				}

				?>
				

			</div>

			<!-- #####  Formulär för registrering #####-->
			<div class="item2">
				<div>
					<h1> Registrera dig här: </h1>
				</div>

			   	<form method="post" action="register.php">	
					<input type="text" name="userreg" placeholder="Användarnamn"><br>	
					<input type="password" name="passreg" placeholder="Lösenord"><br> 	
					<input type="submit" name="register" value="Registrera" class="submit"><br> 	
				</form>	

				<?php

				if( !empty($_SESSION["register"]) ){
					// ##### Om båda fälten i iregistreringsformuläret inte är ifyllda dyker en varningstext upp #####
					if ($_SESSION["register"] == "notset") {  
						echo "<p class='warning'>Registreringen misslyckades.<br> Fyll i både användarnamn och lösenord!</p>";
						$_SESSION["register"]="done";	
					}
				}

				?>

			</div>

		</div>

    </body>

</html>
