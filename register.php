<?php
session_start();
require "database.php";


// För att kolla så att både användarnamn och löserord är ifyllt.
// Om inte så sker omdirigering till index.php
if( isset($_POST["register"]) ) {

	if( empty($_POST["userreg"]) || empty($_POST["passreg"]) ) {
		$_SESSION["register"] = "notset";
		header ("Location: index.php");
	}
}


// Ansluter till databas
$conn = new mysqli($server, $username, $password, $database);
// Kollar att anslutningen fungerar
if ($conn->connect_error) {
    die("Anslutningen misslyckades: " . $conn->connect_error);
} 


if ( !empty($_POST["userreg"]) && !empty($_POST["passreg"]) ) {

		// Tar bort eventuella skadliga HTML- eller PHP- taggar 
		$ur = strip_tags($_POST["userreg"]);
		$pr = strip_tags($_POST["passreg"]); 
		// Krypterar lösenordet
		$pr_hash = password_hash($pr , PASSWORD_BCRYPT); 
		
		// Query till databasen om insättning i tabellen members
		$query = "INSERT INTO members (id, username, password)
				VALUES (NULL, '$ur', '$pr_hash')";

		// Om insättningen i tabellen fungerar så görs en ny query för att 
		// hämta id.		
		if ($conn->query($query) === TRUE) {

			$query = " SELECT * FROM members WHERE password = '{$pr_hash}' ";
			$stmt = $conn->stmt_init();
	
			if( $stmt->prepare($query) ){
					$stmt->execute();
					$stmt->bind_result($id, $ur, $up, $upic);
					$stmt->fetch();
					
			$_SESSION["newuser"] =	TRUE;
			$_SESSION["username"] =	$ur;
			$_SESSION["logged_in"] = NULL;
			$_SESSION["userpic"] = NULL;
			$_SESSION["userid"] = $id;
			setcookie( "username", $ur, time() + 3600 );
			header("Location: dashboard.php");
		}
		} else {
		    echo "Något gick fel: " . $conn->error();
		}

}

$conn->close();

?>
