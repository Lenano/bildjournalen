<?php 
session_start();
// Lägger till html-kod för övre delen av dokumentet (inkl. headern) 
$title = "Medlemssida";
include "top.php";
include "database.php";

?>

	<a href="logout.php"> <p class="logout">Logga ut</p> </a>

	<div class="container"> 

	    <div class="item3">	

	<?php

	if( isset($_COOKIE["username"]) && $_COOKIE["username"] == TRUE ) {

	if( isset($_SESSION["logged_in"] ) && $_SESSION["logged_in"] == TRUE ){
       
        	$un = $_SESSION["username"];
        	echo "<h2 class='welcome_text'>Du är inloggad som " . $un . "</h2>";
			//echo "<img src='$userpic' alt='Profilbild på' " . $un . ">";
		}

	if( isset($_SESSION["newuser"]) && $_SESSION["newuser"] == TRUE){
		echo "<h2 class='welcome_text'> Välkommen " . $_SESSION["username"] . "!</h2>
		<p>Du är nu registrerad och kan ladda upp en bild om du vill.</p>";
		
	}


    if( isset($_POST["upload"]) ){

		$userid = $_SESSION["userid"];
		//echo $userid;
	
		$conn = new mysqli($server, $username, $password, $database);

		if($conn->connect_errno) {
			echo "Ansluter ej till databas<br>";
		}

	

		// Definerar variablar med namn på bildmappen och på sökvägen dit
		$target_folder = "userpics/";
		// Döper om bilden till nytt namn och filändelse för att ta bort ev skadlig kod
		$target_name = $target_folder . time() . basename("user-" . "$userid" . ".jpg");

		if($_FILES["profile_pic"]["size"] > 5000000){
			echo "Bilden är för stor";
			exit;
		}

		$type = strtolower( pathinfo($target_name, PATHINFO_EXTENSION) );
		if($type == "jpg" || $type == "jpeg") {
			//echo "Rätt filformat!<br>";
		}else{
			echo "Fel filformat, endast jpg/jpeg-filer accepteras.";
			exit;
		}
		

		if( move_uploaded_file($_FILES["profile_pic"]["tmp_name"], $target_name) ){
			//echo "uppladdning till lokal mapp OK!<br>";


	
			$query = "UPDATE members SET userpic_url = '{$target_name}' WHERE id = '{$userid}' ";

			
			
			$stmt = $conn->stmt_init();


			if( $stmt->prepare($query) ){
				$stmt->execute();
				//echo "Bilden är uppladdad till databas.";
				
			}else{
				echo mysqli_error($conn);
			}

		}
	
	
	
		$query = " SELECT * FROM members WHERE id = '{$userid}' ";
		$stmt = $conn->stmt_init();
	
		if( $stmt->prepare($query) ){
					$stmt->execute();
					$stmt->bind_result($id, $un, $up, $upic);
					$stmt->fetch();
					$_SESSION["userpic"] = $upic;
					//echo "Bild hämtad från db" . $un . "<br>";
					//echo "<img src=" . $upic . " alt= 'profilbild'> ";
					
		}

		$conn->close();	 		
	}			

	

	?>
			</div>

			<div class="item4">	
				
				<img class='profilpic' src=" <?php echo $_SESSION["userpic"] ?> ">

    			<form method="post" enctype="multipart/form-data">
					<input type="submit" name="upload" value="ladda upp bild" class="uploadbutton">
					<input type="file" name="profile_pic" class='imagebutton'>
				</form>
				
			</div>
		</div>	

	</body>
</html>

<?php
}else
header ("Location: index.php");

?>

